from contaventasAPI.viewsets import ContaventasViewset
from rest_framework import routers
#generacion de enrutamientos de vistas
router = routers.DefaultRouter()
router.register('Contaventas',ContaventasViewset)
#procedimientos que se pueden hacer y correcciones que se deben de hacer 
#Localhost:p/api/contaventas/5
#GET, POST, PUT, DELETE
#list, retrive
#estos procedimientos de enrutamiento son solo para el api 