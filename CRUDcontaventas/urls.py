"""CRUDcontaventas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.contrib.messages import views
from django.urls import path,include
#from django.views.generic import TemplateView
from contaventasAPI.views import ContaventasListado,ContaventasActualizar,ContaventasCrear,ContaventasDetalle,ContaventasEliminar
#from . import views


from django.conf import settings
from django.conf.urls.static import static 
from .router import router

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/',include(router.urls)),
    path('leer/',ContaventasListado.as_view(template_name = "contaventasAPI/index.html"), name='leer'),
    path('leer/detalles/<int:pk>',ContaventasDetalle.as_view(template_name = "contaventasAPI/detalles.html"), name='detalles'), 
    path('leer/crear', ContaventasCrear.as_view(template_name = "contaventasAPI/crear.html"), name='crear'),
    path('leer/editar/<int:pk>', ContaventasActualizar.as_view(template_name = "contaventasAPI/actualizar.html"), name='editar'),    
    path('leer/eliminar/<int:pk>', ContaventasEliminar.as_view(), name='eliminar'),  
  
    
] #+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#if #settings.DEBUG:
    #urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    #urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    

