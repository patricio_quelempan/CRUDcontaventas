import sqlite3
import time
import pyodbc

def conectar_sqlite3(ruta_bd):
    print(f"Comenzando conexión a base de datos {ruta_bd}...")
    intentos = 3
    for i in range(0, intentos):
        try:
            conexion = sqlite3.connect(ruta_bd)
            if conexion:
                print("Conexión lograda exitosamente")
                return 1, conexion
        except:
            time.sleep(30)
            print(f"Problemas con la conexión. Intento número {i}")
    else:
        msj = "Problemas con la conexión. Por favor revisar"
        return 0, msj

def close_sqlite3(conexion):
    conexion.close()
    print("conexion a sqlite3 cerrada exitosamente")

def query_sqlite3(conexion, sql_query):
    print("Realizando Consulta...")
    query = conexion.cursor()
    query.execute(sql_query)
    filas = query.fetchall()
    query.close
    return filas


def insert_sqlite3(conexion, sql_query, entities):

    print("Comienzo de insert en base de datos")
    try:
        insert = conexion.cursor()
        insert.execute(sql_query, entities)
        conexion.commit()
        return 1
    except:
        print("Error al insertar nuevos datos")
        return 0

def update_sqlite3(conexion, sql_query, entities):
    print("Comienzo update en base de datos")
    try:
        update = conexion.cursor()
        update.execute(sql_query, entities)
        conexion.commit()
        return 1
    except:
        print("Error al actualizar datos")
        return 0

def conectar_sybase():
    print(f"Comenzando conexión a base de datos Sybase...")
    intentos = 3
    for i in range(0, intentos):
        try:
            conn = pyodbc.connect(DSN='sybaseiq')
            if conn:
                print("Conexión lograda exitosamente")
                return 1, conn
        except:
            time.sleep(30)
            print(f"Problemas con la conexión. Intento número {i}")
    else:
        msj = "Problemas con la conexión. Por favor revisar"
        return 0, msj
