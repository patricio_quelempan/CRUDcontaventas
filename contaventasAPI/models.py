from django.db import models

# Create your models here.
#crear clase ventas,para registrar las boletas y facturas de compra
#procura respetar siempre los nombres de las variables
class Contaventas(models.Model):
    numero = models.CharField(max_length=100)
    dia   = models.CharField(max_length=2, blank=False)
    documento = models.CharField(max_length=20, blank=False)
    fecha   = models.DateTimeField("fecha", null=False)
    cliente = models.CharField(max_length=100)
    neto    = models.CharField(max_length=20)
    iva     = models.CharField(max_length=20)
    total   = models.CharField(max_length=20)
    

class Meta:
         db_table = 'Contaventas' # Le doy de nombre 'contaventas' a nuestra tabla en la Base de Datos
         

def __str__(self):
     return self.numero
         
#estas son las configuraciones del modelo del api y del scrud
    
#Create / Insert / Add - GET
#Retrieve / Fetch - GET
#Update / Edit - PUT
#Delete / Remove -DELETE 