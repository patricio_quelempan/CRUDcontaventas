# api <-> mobile app/ web app/ etc. json
from django.db.models.fields import Field
from rest_framework import serializers
from .models import Contaventas

#clase de serializers basada en al api
class ContaventasSerializers(serializers.ModelSerializer):
    class Meta:
        model = Contaventas
        fields = '__all__'
    
#para que funciones la lista se debe colocar fields en vez de field y 
#la opcion all,debe de ser colocada con 2 guion bajos adelante y al final