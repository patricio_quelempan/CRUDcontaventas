from django.shortcuts import render
import requests
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import Contaventas
 
from django.urls import reverse
 
from django.contrib import messages 
from django.contrib.messages.views import SuccessMessageMixin 
 
from django import forms
 
class ContaventasListado(ListView): 
    model = Contaventas
 
class ContaventasDetalle(DetailView): 
    model = Contaventas
 
class ContaventasCrear(SuccessMessageMixin, CreateView): 
    model = Contaventas
    form = Contaventas
    fields = "__all__" 
    success_message = 'Registro Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Registro     
 
    # Redireccionamos a la página principal luego de crear un registro
    def get_success_url(self):        
        return reverse('leer') # Redireccionamos a la vista principal 'leer' 
   
class ContaventasActualizar(UpdateView): 
    model = Contaventas
    form = Contaventas
    fields = "__all__" 
    success_message = 'Registro Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Postre 

    # Redireccionamos a la página principal luego de actualizar un registro o postre
    def get_success_url(self):               
        return reverse('leer') # Redireccionamos a la vista principal 'leer' 
 
class ContaventasEliminar(SuccessMessageMixin, DeleteView): 
    model = Contaventas
    form = Contaventas
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro
    def get_success_url(self): 
        success_message = 'Registro Eliminado Correctamente !' # Mostramos este Mensaje luego de Editar un registro
        messages.success (self.request, (success_message))       
        return reverse('leer') # Redireccionamos a la vista principal 'leer'
    

