from rest_framework import viewsets
from .import models
from .import serializers

class ContaventasViewset(viewsets.ModelViewSet):
    queryset = models.Contaventas.objects.all()
    serializer_class = serializers.ContaventasSerializers

#estas son solo las vistas del api    
#list(), retrieve(), create(), update(), destroy()
