#Código para cargar diferencia de pedidos
####################################################
from numpy.core.arrayprint import IntegerFormat
import pandas as pd
#import librerias.api_gmail as gmail
import sys
import os.path
import time
from datetime import datetime 
from dateutil import parser
from pandas.core.frame import DataFrame
from urllib3.exceptions import NewConnectionError
from urllib3.exceptions import MaxRetryError
import sqlite3
import api_bases_datos as sql3
from pandas_ods_reader import read_ods
import numpy as np
from unidecode import unidecode
import pathlib as pt
from openpyxl.workbook import Workbook
from openpyxl.styles import NamedStyle, Font, Border, Side, Fill, Alignment
from openpyxl.styles import Color, PatternFill, Font, Border, colors
from openpyxl.reader.excel import load_workbook
from openpyxl.cell import Cell
import openpyxl
from seaborn import load_dataset
import json
import requests
import pdb

#input
ruta_bd = "D:\\phito\\Documents\\Estudios\\DuocUC\\IngInformatica\\IntegracionPlataformas\\IntPlataformas_Git\\Ventas_API-master\\pyAutoContabilidad\\pyAutoContabilidad_bdd\\db.sqlite3"
conexion = sqlite3.connect(ruta_bd)
api_url = "http://127.0.0.1:8000/products/" #SOLICITAR API ENDPOINT A AREA VENTAS


#Iniciar base de datos proyecto contabilidad
def trabajar_datos_cargados_contabilidad_ventas(ruta_bd):
    print("Comenzando a trabajar datos cargados en proy contabilidad ")
    conect_sqlite = sql3.conectar_sqlite3(ruta_bd)
    if conect_sqlite[0] == 1:
        sql_contaventas = """select * from contaventasAPI_contaventas"""
        sql_get_contaventas = sql3.query_sqlite3(conect_sqlite[1], sql_contaventas)
        print("Select from sqlite3 contabilidad tabla ventas: ", sql_get_contaventas)
        #print(f"Monto_Total: {len(sql_get_contaventas)}")
        df_tabla_contaventas = pd.read_sql(sql_contaventas, conect_sqlite[1])
        #En caso de que se deban dejar solicitudes en estado leida
        if len(sql_get_contaventas) >= 1:
            print("Existe data en monto_Total contaventas")
            #print("df_tabla_boletas: ", df_tabla_boletas)
        else:
            print("No existe data en monto_Total contaventas")
    #elif conect_sqlite[0] == 1:

        return df_tabla_contaventas

def trabajar_datos_cargados_contabilidad_ventas_mensual(ruta_bd):
    print("Comenzando a trabajar datos cargados en proy contabilidad ")
    conect_sqlite = sql3.conectar_sqlite3(ruta_bd)
    if conect_sqlite[0] == 1:
        sql_contaventas_mensual = """select * from contaventasAPI_contaventas_mensual"""
        sql_get_contaventas_mensual = sql3.query_sqlite3(conect_sqlite[1], sql_contaventas_mensual)
        print("Select from sqlite3 contabilidad tabla ventas mensual: ", sql_get_contaventas_mensual)
        #print(f"Monto_Total: {len(sql_get_contaventas)}")
        df_tabla_contaventas_mensual = pd.read_sql(sql_contaventas_mensual, conect_sqlite[1])
        #En caso de que se deban dejar solicitudes en estado leida
        if len(sql_get_contaventas_mensual) >= 1:
            print("Existe data en monto_Total contaventas mensual")
            #print("df_tabla_boletas: ", df_tabla_boletas)
        else:
            print("No existe data en monto_Total contaventas mensual")
    #elif conect_sqlite[0] == 1:

        return df_tabla_contaventas_mensual

def trabajar_datos_cargados_contabilidad_facturas(ruta_bd):
    print("Comenzando a trabajar datos cargados en proy contabilidad ")
    conect_sqlite = sql3.conectar_sqlite3(ruta_bd)
    if conect_sqlite[0] == 1:
        sql_facturas = """select * from FACTURAS_VENTA"""
        sql_get_facturas = sql3.query_sqlite3(conect_sqlite[1], sql_facturas)
        print("Select from sqlite3 contabilidad tabla facturas: ", sql_get_facturas)
        print(f"Monto_Total: {len(sql_get_facturas)}")
        df_tabla_facturas = pd.read_sql(sql_facturas, conect_sqlite[1])

        #En caso de que se deban dejar solicitudes en estado leida
        if len(sql_get_facturas) >= 1:
            print("Existe data en monto_Total facturas")
            #print("df_tabla_facturas: ", df_tabla_facturas)
        else:
            print("No existe data en monto_Total facturas")
        return df_tabla_facturas
    
def input_datos_carga_contaventasAPI(conexion, id, numero, dia, documento, fecha, cliente, neto, iva, total):

    entities = (id, numero, dia, documento, fecha, cliente, neto, iva, total)
    sql_query ='''INSERT INTO CONTAVENTASAPI_CONTAVENTAS ("ID", "NUMERO", "DIA", "DOCUMENTO", "FECHA", "CLIENTE", "NETO", "IVA", "TOTAL") VALUES (?,?,?,?,?,?,?,?,?);''' 
    sql_insert = sql3.insert_sqlite3(conexion, sql_query, entities)
    if sql_insert == 1:
        print(f"Fila de Datos insertados exitosamente para conta venta. Se continua con siguiente Fila")
        aux = "ok"         
    else:
        print(f"Problemas al insertar datos para conta venta. Se intentara realizar insert en próxima ejecución")
        #sql3.error_BD()

def input_datos_carga_contaventasAPI_mensual(conexion, id, total_ordenes_compra, total_dias, tipo_documento, fecha, total_mensual):

    entities = (id, total_ordenes_compra, total_dias, tipo_documento, fecha, total_mensual)
    sql_query ='''INSERT INTO CONTAVENTASAPI_CONTAVENTAS_MENSUAL ("ID", "total_ordenes_compra", "total_dias", "tipo_documento", "FECHA", "TOTAL_MENSUAL") VALUES (?,?,?,?,?,?);''' 
    sql_insert = sql3.insert_sqlite3(conexion, sql_query, entities)
    if sql_insert == 1:
        print(f"Fila de Datos insertados exitosamente para conta ventas mensual. Se continua con siguiente Fila")
        aux = "ok"         
    else:
        print(f"Problemas al insertar datos para conta ventas mensual. Se intentara realizar insert en próxima ejecución")
        #sql3.error_BD()

def import_postman_api_request(api_url):
    payload = ""
    url = api_url
    headers = {
    'Cookie': 'csrftoken=98c8x9PTBSTuvuxQGAgNG2op9q6FlnyZh5b0XpMC3hTRySILFg5cH07kNubDiGmL'
    }
    response = requests.request("GET", url, headers=headers, data=payload, verify= False)
    response_text = response.text
    # using json.loads()
    # convert dictionary string to dictionary
    res = json.loads(response_text)   
    list(res[1].keys())
    list(res[1].values())
    columns_ventas = list(res[1].keys())
    values_ventas = list(res[1].values())
    df_datos_ventas = pd.DataFrame([values_ventas], columns = columns_ventas)
    return df_datos_ventas

#INICIO Metodos
df_tabla_contaventas_sqlite3= trabajar_datos_cargados_contabilidad_ventas(ruta_bd)
#df_tabla_facturas_sqlite3= trabajar_datos_cargados_contabilidad_facturas(ruta_bd)
df_tabla_comtaventas_mensual_sqlite3= trabajar_datos_cargados_contabilidad_ventas_mensual(ruta_bd)
df_api_ventas= import_postman_api_request(api_url)
print("df_tabla_contaventas_sqlite3: ", df_tabla_contaventas_sqlite3)
print("df_tabla_contaventas_sqlite3: ", df_tabla_comtaventas_mensual_sqlite3)
print("df_api_ventas: ", df_api_ventas)

#pdb.set_trace()
#datos para input tabla contaventas
id = int(df_tabla_contaventas_sqlite3.loc[1, "id"])+7
print("id: ", id)
numero_orden = int(df_api_ventas.loc[1, "order"])
numero_orden= str(numero_orden)
print("numero orden: ", numero_orden)
dia= 1
dia = str(dia)
print("dia: ", dia)
documento= "boleta"
documento= str(documento)
print("documento: ", documento)
fecha= df_api_ventas.loc[0, "timestamp"]
print("fecha: ", fecha)
cliente= df_tabla_contaventas_sqlite3.loc[1, "cliente"] 
cliente= str(cliente)
print("cliente: ", cliente)
total= int(df_api_ventas.loc[0, "amount"]) 
iva= round(total*19/100)
neto= total-iva
total= str(total)
iva= str(iva)
neto= str(neto)
print("total: ", total)
print("iva: ", iva)
print("neto: ", neto)

#input_datos_cargados_contabilidad_ventas(conexion, id, dia_contable, tipo_documento, numero_documento, id_cliente, monto_venta, afecto_iva, monto_total)
input_datos_carga_contaventasAPI(conexion, id, numero_orden, dia, documento, fecha, cliente, neto, iva, total)
sql3.close_sqlite3(conexion)
time.sleep(5)
#volver a cargar datos tabla contaventasAPI_contaventas
df_tabla_contaventas_sqlite3= trabajar_datos_cargados_contabilidad_ventas(ruta_bd)
#df_tabla_facturas_sqlite3= trabajar_datos_cargados_contabilidad_facturas(ruta_bd)
print("df_tabla_contaventas_sqlite3: ", df_tabla_contaventas_sqlite3)

#pasar a reporte diario tabla contabilidad
dir_respaldo_arc= r"D:\\phito\Documents\\Estudios\DuocUC\\IngInformatica\\IntegracionPlataformas\\IntPlataformas_Git\\Ventas_API-master\\pyAutoContabilidad\\pyAutoContabilidad_reportes_diario\\"
n_respaldo_arc = "reporte_diario_contab"
n_respaldo_arc = n_respaldo_arc+"_"+time.strftime("%d%m%Y_%H%M%S") + ".xlsx"
df_tabla_contaventas_sqlite3.to_excel(fr'''{dir_respaldo_arc}\{n_respaldo_arc}''', header=True, index=False)

###################################################################################################################################
time.sleep(10)
print("INICIO proceso conta ventas mensual")
#datos para input tabla contaventas mensual
id = int(df_tabla_contaventas_sqlite3.loc[1, "id"])+1
print("id: ", id)
total_ordenes_compra = int(df_tabla_contaventas_sqlite3["numero"].count())
total_ordenes_compra= str(total_ordenes_compra)
print("numero orden: ", total_ordenes_compra)
total_dias= 1*30
total_dias = str(total_dias)
print("total dias: ", total_dias)
documento_boleta= "boleta"
documento_boleta= str(documento_boleta)
documento_factura= "factura"
documento_factura= str(documento_factura)
print("documento boleta: ", documento_boleta)
print("documento factura: ", documento_factura)
tipo_documento = documento_boleta
fecha= datetime.now()
print("fecha: ", str(fecha))
total_mensual= int(df_tabla_contaventas_sqlite3["total"].sum())
print("total mensual: ", total_mensual)


#input_datos_cargados_contabilidad_ventas mensual(conexion, id, dia_contable, tipo_documento, numero_documento, id_cliente, monto_venta, afecto_iva, monto_total)
input_datos_carga_contaventasAPI_mensual(conexion, id, total_ordenes_compra, total_dias, tipo_documento, fecha, total_mensual)
sql3.close_sqlite3(conexion)
time.sleep(5)
#volver a cargar datos tabla contaventasAPI_contaventas
df_tabla_contaventas_mensual_sqlite3= trabajar_datos_cargados_contabilidad_ventas_mensual(ruta_bd)
#df_tabla_facturas_sqlite3= trabajar_datos_cargados_contabilidad_facturas(ruta_bd)
print("df_tabla_contaventas_mensual_sqlite3: ", df_tabla_contaventas_mensual_sqlite3)

#pasar a reporte diario tabla contabilidad
dir_respaldo_arc_mensual= r"D:\\phito\Documents\\Estudios\DuocUC\\IngInformatica\\IntegracionPlataformas\\IntPlataformas_Git\\Ventas_API-master\\pyAutoContabilidad\\pyAutoContabilidad_reportes_diario_mensual\\"
n_respaldo_arc_mensual = "reporte_mensual_contab"
n_respaldo_arc_mensual = n_respaldo_arc_mensual+"_"+time.strftime("%d%m%Y_%H%M%S") + ".xlsx"
df_tabla_contaventas_mensual_sqlite3.to_excel(fr'''{dir_respaldo_arc_mensual}\{n_respaldo_arc_mensual}''', header=True, index=False)

print("FIN proceso py Contabilidad API Ventas")
sys.exit()