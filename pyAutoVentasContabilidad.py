# Python3 code to demonstrate
# convert dictionary string to dictionary
# using json.loads()
import json
import requests
import os
import pandas as pd
import pdb


url = "http://127.0.0.1:8000/products/"

payload = ""
headers = {
  'Cookie': 'csrftoken=98c8x9PTBSTuvuxQGAgNG2op9q6FlnyZh5b0XpMC3hTRySILFg5cH07kNubDiGmL'
}

response = requests.request("GET", url, headers=headers, data=payload, verify= False)

response_text = response.text

# using json.loads()
# convert dictionary string to dictionary
res = json.loads(response_text)
  
# print result
print("The converted dictionary : " + str(res))

print(type(res))

print(res[0])

print(type(res[0]))

print("keys: ",res[0].keys())
print("values: ",res[0].values())

list(res[0].keys())
list(res[0].values())
columns_ventas = list(res[0].keys())
values_ventas = list(res[0].values())
print("columns_ventas: ", str(columns_ventas))
print("values_ventas: ", str(values_ventas))
type(columns_ventas)
type(values_ventas)
pdb.set_trace()


df_datos_ventas = pd.DataFrame([values_ventas], columns = columns_ventas)
print (df_datos_ventas)
print (type(df_datos_ventas))



'''for i in res[0]:
        print("title: ", i['title'])
        print()
for i in res['people1']:
        print("Name:", i['name'])

def Convert(res):
    res_dct = {res[i]: res[i + 1] for i in range(0, len(res), 2)}
    return res_dct

print(Convert(res))

print(type(Convert(res)))'''